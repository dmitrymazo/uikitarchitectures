//
//  StopWatchViewModel.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation

protocol StopWatchViewModelProtocol {
    // Output
    var time: TimeInterval { get } // Observable!!!
    var isStartButtonHidden: Observable<Bool> { get }
    var isPauseButtonHidden: Observable<Bool> { get }
    var isResumeButtonHidden: Observable<Bool> { get }
    var isResetButtonHidden: Observable<Bool> { get }
    
    // Input
    func start()
    func pause()
    func resume()
    func reset()
}

final class StopWatchViewModel: StopWatchViewModelProtocol {
    
    var time: TimeInterval {
        let elapsed = timeMeasurer.elapsed() ?? 0
        return previousTime + elapsed
    }
    
    private(set) var isStartButtonHidden = Observable(false)
    private(set) var isPauseButtonHidden = Observable(true)
    private(set) var isResumeButtonHidden = Observable(true)
    private(set) var isResetButtonHidden = Observable(true)
    
    private var previousTime = 0.0
    private var state = StopWatchState.uninitialized
    private let timeMeasurer: TimeMeasurerProtocol
    
    private func updateButtonVisibility() {
        switch state {
        case .uninitialized:
            isStartButtonHidden.value = false
            isPauseButtonHidden.value = true
            isResumeButtonHidden.value = true
            isResetButtonHidden.value = true
        case .started,
             .resumed:
            isStartButtonHidden.value = true
            isPauseButtonHidden.value = false
            isResumeButtonHidden.value = true
            isResetButtonHidden.value = false
        case .paused:
            isStartButtonHidden.value = true
            isPauseButtonHidden.value = true
            isResumeButtonHidden.value = false
            isResetButtonHidden.value = false
        }
    }
    
    func start() {
        guard state == .uninitialized else { return }
        state = .started
        updateButtonVisibility()
        timeMeasurer.start()
    }
    
    func pause() {
        guard [.started, .resumed].contains(state) else { return }
        state = .paused
        previousTime = timeMeasurer.elapsed() ?? 0
        updateButtonVisibility()
        timeMeasurer.reset()
    }
    
    func resume() {
        guard state == .paused else { return }
        state = .resumed
        updateButtonVisibility()
        timeMeasurer.start()
    }
    
    func reset() {
        state = .uninitialized
        previousTime = 0
        timeMeasurer.reset()
    }
    
    init(timeMeasurer: TimeMeasurerProtocol) {
        self.timeMeasurer = timeMeasurer
    }
    
}

private enum StopWatchState {
    case uninitialized
    case started
    case paused
    case resumed
}
