//
//  TimeMeasurer.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation

protocol TimeMeasurerProtocol {
    func start()
    func reset()
    func elapsed() -> TimeInterval?
}

final class TimeMeasurer: TimeMeasurerProtocol {
    
    private var startTime: DispatchTime?
    
    func start() {
        self.startTime = DispatchTime.now()
    }
    
    func reset() {
        self.startTime = nil
    }
    
    func elapsed() -> TimeInterval? {
        guard let startTime = self.startTime else { return nil }
        let endTime = DispatchTime.now()
        let diff = endTime.uptimeNanoseconds - startTime.uptimeNanoseconds
        let nanoSecInSec = 1_000_000_000.0
        
        return TimeInterval(diff) / nanoSecInSec
    }
    
}
