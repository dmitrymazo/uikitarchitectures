//
//  Observable.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation

final class Observable<Value> {
    
    private var closure: ((Value) -> ())?

    public var value: Value {
        didSet { closure?(value) }
    }

    public init(_ value: Value) {
        self.value = value
    }

    public func observe(_ closure: @escaping (Value) -> Void) {
        self.closure = closure
        closure(value)
    }
}
