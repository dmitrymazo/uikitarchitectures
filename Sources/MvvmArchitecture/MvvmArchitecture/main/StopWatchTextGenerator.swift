//
//  StopWatchTextGenerator.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation

final class StopWatchTextGenerator {
    static func text(from time: TimeInterval) -> String {
        let timeObj = Time(seconds: time)
        
        let minutes = String(format: "%02d", timeObj.minutes)
        let seconds = String(format: "%02d", timeObj.seconds)
        let milliSeconds = String(format: "%02d", timeObj.milliSeconds)
        
        return "\(minutes):\(seconds),\(milliSeconds)"
    }
}

struct Time {
    let hours: Int
    let minutes: Int
    let seconds: Int
    let milliSeconds: Int
    
    init(hours: Int, minutes: Int, seconds: Int, milliSeconds: Int = 0) {
        self.hours = hours
        self.minutes = minutes
        self.seconds = seconds
        self.milliSeconds = milliSeconds
    }
    
    init(seconds: TimeInterval) {
        let decimalSeconds = Int(seconds)
        
        let milliSecondsInSecond = 1000.0
        let milliSeconds = seconds.truncatingRemainder(dividingBy: 1) * milliSecondsInSecond
        
        self.hours = decimalSeconds / 3600
        self.minutes = (decimalSeconds % 3600) / 60
        self.seconds = (decimalSeconds % 3600) % 60
        self.milliSeconds = Int(milliSeconds)
    }
}
