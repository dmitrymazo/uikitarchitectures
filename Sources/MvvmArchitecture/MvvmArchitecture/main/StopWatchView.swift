//
//  StopWatchView.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import UIKit

final class StopWatchView: UIView {
    let timeLabel = TimeLabel()
    let controlPanel = ControlPanelView()
    
    init() {
        super.init(frame: .zero)
        
        let spacingConstraint = controlPanel.topAnchor.constraint(equalTo: timeLabel.bottomAnchor, constant: 30)
        spacingConstraint.priority = UILayoutPriority(500)
        
        NSLayoutConstraint.activate([
            timeLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 80),
            timeLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            //
            spacingConstraint,
            controlPanel.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor, constant: -20)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class TimeLabel: UILabel {
    init() {
        super.init(frame: .zero)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        self.font = UIFont.systemFont(ofSize: 30)
        self.textColor = .gray
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class ControlPanelView: UIView {
    let startButton: UIButton = {
        let button = UIButton()
        button.setTitle("Start", for: .normal)
        
        return button
    }()
    
    let pauseButton: UIButton = {
        let button = UIButton()
        button.setTitle("Pause", for: .normal)
        
        return button
    }()
    
    let resumeButton: UIButton = {
        let button = UIButton()
        button.setTitle("Resume", for: .normal)
        
        return button
    }()
    
    let resetButton: UIButton = {
        let button = UIButton()
        button.setTitle("Reset", for: .normal)
        
        return button
    }()
    
    private let stackview = UIStackView()
    
    init() {
        super.init(frame: .zero)
        
        stackview.addArrangedSubview(startButton)
        stackview.addArrangedSubview(pauseButton)
        stackview.addArrangedSubview(resumeButton)
        stackview.addArrangedSubview(resetButton)
        
        stackview.axis = .horizontal
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
