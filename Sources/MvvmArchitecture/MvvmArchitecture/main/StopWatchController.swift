//
//  StopWatchController.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation

final class StopWatchController {
    private let viewModel: StopWatchViewModelProtocol
    private let contentView = StopWatchView()
    
    private func bind() {
        //        self.viewModel.time.observe { [weak self] time in
        //            DispatchQueue.main.async {
        //                self?.contentView.timeLabel.text = StopWatchTextGenerator.text(from: time)
        //            }
        //        }
        
        self.viewModel.isStartButtonHidden.observe { [weak self] isHidden in
            DispatchQueue.main.async {
                self?.contentView.controlPanel.startButton.isHidden = isHidden
            }
        }
        
        self.viewModel.isPauseButtonHidden.observe { [weak self] isHidden in
            DispatchQueue.main.async {
                self?.contentView.controlPanel.pauseButton.isHidden = isHidden
            }
        }
        
        self.viewModel.isResumeButtonHidden.observe { [weak self] isHidden in
            DispatchQueue.main.async {
                self?.contentView.controlPanel.resumeButton.isHidden = isHidden
            }
        }
        
        self.viewModel.isResetButtonHidden.observe { [weak self] isHidden in
            DispatchQueue.main.async {
                self?.contentView.controlPanel.resetButton.isHidden = isHidden
            }
        }
    }
    
    private func setupEvents() {
        contentView.controlPanel.startButton.addTarget(self, action: #selector(startTapped), for: .touchUpInside)
        contentView.controlPanel.pauseButton.addTarget(self, action: #selector(pauseTapped), for: .touchUpInside)
        contentView.controlPanel.resumeButton.addTarget(self, action: #selector(resumeTapped), for: .touchUpInside)
        contentView.controlPanel.resetButton.addTarget(self, action: #selector(resetTapped), for: .touchUpInside)
    }
    
    @objc
    private func startTapped() {
        viewModel.start()
    }
    
    @objc
    private func pauseTapped() {
        viewModel.pause()
    }
    
    @objc
    private func resumeTapped() {
        viewModel.resume()
    }
    
    @objc
    private func resetTapped() {
        viewModel.reset()
    }
    
    init(viewModel: StopWatchViewModelProtocol) {
        self.viewModel = viewModel
        self.bind()
        self.setupEvents()
    }
}
