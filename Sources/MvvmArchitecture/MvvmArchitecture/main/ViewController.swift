//
//  ViewController.swift
//  MvvmArchitecture
//
//  Created by Dmitry Mazo on 5/5/21.
//

import UIKit
import Common

final class ViewController: UIViewController {
    
    private let viewModel: MainViewModelProtocol
    private let contentView = MainView()
    
    private let yearsOfExperience: UInt = 2
    
    // MARK: - Private
    
    @objc
    private func tapped() {
//        calculateSalary()
    }
    
    // MARK: - Internal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(contentView)
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.view.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
        
        contentView.button.addTarget(self, action: #selector(tapped), for: .touchUpInside)
    }
    
    init(viewModel: MainViewModelProtocol) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}



