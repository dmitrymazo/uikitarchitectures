//
//  MockTimeMeasurer.swift
//  MvvmArchitectureTests
//
//  Created by Dmitry Mazo on 5/8/21.
//

import Foundation
@testable
import MvvmArchitecture

final class MockTimeMeasurer: TimeMeasurerProtocol {
    private(set) var isRunning = false
    
    private var elapsedTime = 0.0
    
    func start() {
        isRunning = true
    }
    
    func reset() {
        elapsedTime = 0
        isRunning = false
    }
    
    func elapsed() -> TimeInterval? {
        return elapsedTime
    }
    
    // MARK: - Mock
    
    func setElapsed(_ time: TimeInterval) {
        elapsedTime = time
    }
}
