//
//  StopWatchTimeTests.swift
//  MvvmArchitectureTests
//
//  Created by Dmitry Mazo on 5/8/21.
//

import XCTest
@testable
import MvvmArchitecture

final class StopWatchTimeTests: XCTestCase {
    
    private var timeMeasurer = MockTimeMeasurer()
    private var stopWatch: StopWatchViewModel!
    
    override func setUp() {
        stopWatch = StopWatchViewModel(timeMeasurer: timeMeasurer)
    }
    
    func testInitialTimeIsZero() {
        XCTAssertEqual(stopWatch.time, 0)
    }
    
    func testStartComponent() {
        stopWatch.start()
        XCTAssertEqual(timeMeasurer.isRunning, true)
    }
    
    func testPauseComponent() {
        stopWatch.start()
        timeMeasurer.setElapsed(10)
        stopWatch.pause()
        
        XCTAssertEqual(timeMeasurer.isRunning, false)
        XCTAssertEqual(stopWatch.time, 10)
    }
    
    func testResumeComponent() {
        stopWatch.start()
        timeMeasurer.setElapsed(30)
        stopWatch.pause()
        stopWatch.resume()
        timeMeasurer.setElapsed(10)
        
        XCTAssertEqual(timeMeasurer.isRunning, true)
        XCTAssertEqual(stopWatch.time, 40)
    }
    
    func testResetComponent() {
        stopWatch.start()
        timeMeasurer.setElapsed(20)
        
        stopWatch.reset()
        XCTAssertEqual(timeMeasurer.isRunning, false)
        XCTAssertEqual(stopWatch.time, 0)
    }
    
}
