//
//  TimeTests.swift
//  MvvmArchitectureTests
//
//  Created by Dmitry Mazo on 5/8/21.
//

import XCTest
@testable
import MvvmArchitecture

final class TimeTests: XCTestCase {

    func testTime() {
        let time = Time(seconds: 4205.3)
        
        XCTAssertEqual(time.hours, 1)
        XCTAssertEqual(time.minutes, 10)
        XCTAssertEqual(time.seconds, 5)
        XCTAssertEqual(time.milliSeconds, 300)
    }

}
