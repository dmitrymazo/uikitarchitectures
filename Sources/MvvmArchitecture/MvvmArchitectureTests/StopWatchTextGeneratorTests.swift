//
//  StopWatchTextGeneratorTests.swift
//  MvvmArchitectureTests
//
//  Created by Dmitry Mazo on 5/8/21.
//

import XCTest
@testable
import MvvmArchitecture

final class StopWatchTextGeneratorTests: XCTestCase {
    
    func test0() {
        let timeString = StopWatchTextGenerator.text(from: 0)
        XCTAssertEqual(timeString, "00:00,00")
    }
    
    func test10Sec() {
        let timeString = StopWatchTextGenerator.text(from: 10)
        XCTAssertEqual(timeString, "00:10,00")
    }
    
    func test45Sec30Ms() {
        let timeString = StopWatchTextGenerator.text(from: 45.03)
        XCTAssertEqual(timeString, "00:45,30")
    }
    
    func test65Sec6Ms() {
        let timeString = StopWatchTextGenerator.text(from: 65.006)
        XCTAssertEqual(timeString, "01:05,06")
    }
    
}
