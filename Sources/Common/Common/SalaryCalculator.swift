//
//  SalaryCalculator.swift
//  MvcArchitecture
//
//  Created by Dmitry Mazo on 5/5/21.
//

import Foundation

final public class SalaryCalculator {
    static func salary(forYears years: UInt) -> Double {
        switch years {
        case 0..<1:
            return 50
        case 1..<3:
            return 100
        default:
            return 200
        }
    }
}
