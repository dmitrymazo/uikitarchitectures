//
//  MainView.swift
//  Common
//
//  Created by Dmitry Mazo on 5/5/21.
//

import UIKit

final public class MainView: UIView {
    
    public let button: UIButton = {
        let button = UIButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Calculate", for: .normal)
        
        return button
    }()
    
    public let label: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = .white
        
        return label
    }()
    
    public init() {
        super.init(frame: .zero)
        
        self.backgroundColor = .blue
        self.addSubview(button)
        self.addSubview(label)
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            button.topAnchor.constraint(equalTo: self.topAnchor, constant: 50),
            label.topAnchor.constraint(equalTo: button.topAnchor, constant: 50),
            button.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            label.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
