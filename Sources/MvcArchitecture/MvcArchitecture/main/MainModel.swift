//
//  MainModel.swift
//  MvcArchitecture
//
//  Created by Dmitry Mazo on 5/7/21.
//

import Foundation

protocol MainModelProtocol {
    
}

final class MainModel: MainModelProtocol {
    
}
