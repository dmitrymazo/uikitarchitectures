//
//  ViewController.swift
//  MvcArchitecture
//
//  Created by Dmitry Mazo on 5/5/21.
//

import UIKit
import Common

final class ViewController: UIViewController {
    
    private let viewModel: MainModelProtocol
    private let contentView = MainView()
    
    // MARK: - Private
    
    private func calculateSalary() {
        let salary = SalaryCalculator.salary(forYears: yearsOfExperience)
        contentView.label.text = "\(salary)"
    }
    
    @objc
    private func tapped() {
        calculateSalary()
    }
    
    // MARK: - Internal
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(contentView)
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: self.view.topAnchor),
            contentView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            contentView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            contentView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor)
        ])
        
        contentView.button.addTarget(self, action: #selector(tapped), for: .touchUpInside)
    }
    
    // MARK: - Init
    
    init(viewModel: MainModelProtocol) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

